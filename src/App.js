import React from 'react';
import logo from './logo.svg';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import { SnackbarProvider } from 'notistack';

import GatewayList from './componentes/gateway/GatewayList';
import GatewayEdit from './componentes/gateway/GatewayEdit';
import GatewayAdd from './componentes/gateway/GatewayAdd';

function App() {
  
  return (
    <SnackbarProvider
        maxSnack={3}
        anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
      >
    <div>
      <Router>                  
      <header>
      <nav>
          <ul>
            <li>
              <Link to="/gateway-list">Gateways</Link>
            </li>
          </ul>
        </nav>
      </header>
      <main>
        <Switch>
          <Route path="/gateway-list" component={GatewayList} />
          <Route path="/gateway-add" component={GatewayAdd} />
          <Route path="/gateway-edit/:id" component={GatewayEdit} />
        </Switch>
      </main>
    </Router>
    </div>
    </SnackbarProvider>
  );
}

export default App;
